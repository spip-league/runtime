<?php declare(strict_types=1);

namespace Spip\Runtime;

use Composer\InstalledVersions;
use Exception;
use RuntimeException;

enum Installation: string
{
    case Legacy = '/spip.php';

    case Standalone = '/public/index.php';

    case Mutualized = '/sites/default/public/index.php';

    private static function getInstallPath(): string
    {
        $rootDir = realpath(InstalledVersions::getRootPackage()['install_path']);

        if (!$rootDir) {
            throw new Exception('Run composer install'); // @todo throws NotInstalledException
        }

        return $rootDir;
    }

    public function getProjectDir(): string
    {
        return self::getInstallPath();
    }

    public function getSiteDir(): string
    {
        $site = self::guessSite();

        return match ($this) {
            self::Legacy, self::Standalone => $this->getProjectDir(),
            self::Mutualized => \is_dir($this->getProjectDir().sprintf('/sites/%s', $site)) ?
                $this->getProjectDir().sprintf('/sites/%s', $site) :
                throw new RuntimeException('MutualizedSiteNotFoundException'),
        };
    }

    public function getWebDir(): string
    {
        return match ($this) {
            self::Legacy => $this->getProjectDir(),
            self::Standalone, self::Mutualized => $this->getSiteDir().'/public'
        };
    }

	public static function guessFromDir(string $directory): self
	{
		return match (true) {
			file_exists($directory.self::Legacy->value) => self::Legacy,
			file_exists($directory.self::Standalone->value) => self::Standalone,
			file_exists($directory.self::Mutualized->value) => self::Mutualized,
			default => throw new RuntimeException('SPIP Not fully installed'),
		};
	}

    public static function fromComposer(): self
    {
        $rootDir = self::getInstallPath();

        return match (true) {
            \file_exists($rootDir.self::Legacy->value) => self::Legacy,
            \file_exists($rootDir.self::Standalone->value) => self::Standalone,
            \file_exists($rootDir.self::Mutualized->value) => self::Mutualized,
            default => throw new RuntimeException('SPIP Not fully installed'), // @todo throws NotInstalledException
        };
    }

    public static function guessSite(): string
    {
        $debug = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $debug = end($debug);
        $script = $debug['file'] ?? '';
        $site = 'default';
        if (preg_match(',sites/(\w+)/public/index.php$,', $script, $matches)) {
            $site = $matches[1];
        }

        return $site;
    }
}
