<?php declare(strict_types=1);

namespace Spip\Runtime;

final class Old
{
    public static function constant(string $name, $default = \null): mixed
    {
        if (defined($name)) {
            return constant($name);
        }

        return $default;
    }

    public static function global(string $name, $default = \null): mixed
    {
        if (isset($GLOBALS[$name])) {
            return $GLOBALS[$name];
        }

        return $default;
    }

}