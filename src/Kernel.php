<?php declare(strict_types=1);

namespace Spip\Runtime;

use Symfony\Component\Filesystem\Path;

class Kernel
{
    private bool $relativePath = false;
    public readonly string $cwd;

    public function __construct(
        public readonly Installation $installation,
        private string $restricted = 'ecrire',
        private string $etc = 'config',
        private string $tmp = 'tmp',
        private string $img = 'IMG',
        private string $var = 'local',
    ) {
        $this->cwd= (string) \getcwd();
    }

    private function getDir(string $dir, ?string $base = null): string
    {
        $base = !\is_null($base) ? $base : $this->installation->getProjectDir();
        $dir = Path::join($base, $dir);
        if ($this->relativePath) {
            $dir = Path::makeRelative($dir, $this->cwd);
        }

        $this->relativePath = false;

        return $dir;
    }

    public function relative(): self
    {
        $this->relativePath = true;

        return $this;
    }

    public function getRootDir(): string
    {
        return $this->getdir('.');
    }

    public function getRestrictedDir(): string
    {
        return $this->getdir($this->restricted);
    }

    public function getTmpDir(): string
    {
        return $this->getdir($this->tmp, $this->installation->getSiteDir());
    }

    public function getEtcDir(): string
    {
        return $this->getdir($this->etc, $this->installation->getSiteDir());
    }

    public function getImgDir(): string
    {
        return $this->getdir($this->img, $this->installation->getWebDir());
    }

    public function getVarDir(): string
    {
        return $this->getdir($this->var, $this->installation->getWebDir());
    }
}