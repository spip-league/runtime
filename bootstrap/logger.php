<?php

use Monolog\Level;
use Monolog\Logger;
use Spip\Component\Logger\Factory;
use Spip\Runtime\Old;

/**
 * @deprecated 5.0 use Level::Emergency
 */
define('_LOG_HS', 0);
/**
 * @deprecated 5.0 use Level::Alert
 */
define('_LOG_ALERTE_ROUGE', 1);
/**
 * @deprecated 5.0 use Level::Critical
 */
define('_LOG_CRITIQUE', 2);
/**
 * @deprecated 5.0 use Level::Error
 */
define('_LOG_ERREUR', 3);
/**
 * @deprecated 5.0 use Level::Warning
 */
define('_LOG_AVERTISSEMENT', 4);
/**
 * @deprecated 5.0 use Level::Notice
 */
define('_LOG_INFO_IMPORTANTE', 5);
/**
 * @deprecated 5.0 use Level::Info
 */
define('_LOG_INFO', 6);
/**
 * @deprecated 5.0 use Level::Debug
 */
define('_LOG_DEBUG', 7);

/**
 * Enregistrement des événements
 *
 * Signature : `spip_log(message[,niveau|type|type.niveau],level)`
 *
 * @example
 *   ```
 *   spip_log($message)
 *   spip_log($message, level: Monolog\Level::Error)
 *   spip_log($message, 'recherche')
 *   spip_log($message, 'recherche', Level::Error)
 *   Les appels ci-dessous sont "deprecated" depuis 5.0
 *   spip_log($message, _LOG_DEBUG)
 *   spip_log($message, 'recherche.'._LOG_DEBUG)
 *   ```
 *
 * @api
 * @link https://programmer.spip.net/spip_log
 *
 * @param mixed           $message Message à consigner
 * @param int|string|null $name    Nom du fichier de log, "spip" par défaut
 * @param Level|null      $level   @since 5.0 Niveau de gravité
 */
function spip_log($message, $name = null, ?Level $level = null): void
{
    static $toMonolog = [
        Level::Emergency,
        Level::Alert,
        Level::Critical,
        Level::Error,
        Level::Warning,
        Level::Notice,
        Level::Info,
        Level::Debug,
    ];
    static $loggers = [];
    preg_match('/^([a-z_]*)\.?(\d)?$/iS', (string) $name, $regs);
    $logFile = 'spip';
    if (isset($regs[1]) && strlen($regs[1])) {
        $logFile = $regs[1];
    }
    if (is_null($level)) {
        if (!isset($regs[2])) {
            $level = parameter('spip.logger.default_level');
        } else {
            trigger_deprecation(
                'spip/runtime',
                '5.0',
                'La notation spip_log(message[,niveau|type.niveau]) est dépréciée.' .
                    ' Utilisez spip_log(message, type, level) à la place.'
            );
            $level = $toMonolog[intval($regs[2])];
        }
    }

    if (!array_key_exists($logFile, $loggers)) {
        /** @var Factory $factory */
        $factory = container()->get('spip.logger.factory');
        $loggers[$logFile] = $factory->createFromFilename($logFile);
    }

    /** @var Logger[] $loggers */
    $loggers[$logFile]->log($level, preg_replace(
        "/\n*$/",
        "\n",
        is_string($message) ? $message : print_r($message, true)
    ));
}

function convertOldConstantsToLogPath(): string
{
    $log_dir = Old::constant('_DIR_LOG')  ?? 'tmp/log/';
    $log_file = Old::constant('_FILE_LOG') ?? 'spip';
    $log_suffix = Old::constant('_FILE_LOG_SUFFIX') ?? '.log';

    return sprintf('%s%s%s', $log_dir, $log_file, $log_suffix);            
}
