<?php

use Spip\Component\Filesystem\FilesystemInterface;

/**
 * Retourne le contenu d'un fichier texte ou gzippé selon l'extension.
 * 
 * Ne passe pas par un verrou flock
 *
 * @param string $fichier Chemin du fichier
 *
 * @return string Contenu du fichier
 */
function spip_file_get_contents(string $fichier): string
{
    return str_ends_with($fichier, '.gz') ?
        implode('', (array) gzfile($fichier)) :
        (string) file_get_contents($fichier);
}

/**
 * Lit un fichier et place son contenu dans le paramètre transmis.
 *
 * Décompresse automatiquement les fichiers `.gz`
 * Passe par un verrou flock "partagé"
 *
 * @param string $fichier Chemin du fichier
 * @param string $contenu Le contenu du fichier sera placé dans cette variable
 * @param array $options
 *     Options tel que :
 *
 *     - 'phpcheck' => 'oui' : vérifie qu'on a bien du php
 *
 * @return bool true si l'opération a réussie, false sinon.
 */
function lire_fichier($fichier, &$contenu, $options = []): bool
{
    static $filesystem = null;

    if (is_null($filesystem)) {
        /** @var FilesystemInterface $filesystem */
        $filesystem = service('spip.filesystem');
    }

    $contenu = $filesystem->read($fichier);
    if (str_ends_with($fichier, '.gz')) {
        $contenu = gzdecode($contenu);
    }

    $ok = true;
    if (isset($options['phpcheck']) && $options['phpcheck'] == 'oui') {
        $ok &= (preg_match(",[?]>\n?$,", $contenu));
    }

    return $ok;
}

/**
 * Écrit un fichier de manière un peu sûre
 *
 * Cette écriture s’exécute de façon sécurisée en posant un verrou sur
 * le fichier avant sa modification. Les fichiers .gz sont compressés.
 * 
 * @see  lire_fichier()
 * @see  supprimer_fichier()
 *
 * @param string $fichier
 *     Chemin du fichier
 * @param string $contenu
 *     Contenu à écrire
 * @param bool $ignorer_echec
 *     - true pour ne pas raler en cas d'erreur
 *     - false affichera un message si on est webmestre
 * @param bool $truncate
 *     Écriture avec troncation ?
 * @return bool
 *     - true si l’écriture s’est déroulée sans problème.
 */
function ecrire_fichier($fichier, $contenu, $ignorer_echec = false, $truncate = true)
{
    static $filesystem = null;

    if (is_null($filesystem)) {
        /** @var FilesystemInterface $filesystem */
        $filesystem = service('spip.filesystem');
    }

    if (str_ends_with($fichier, '.gz')) {
        $contenu = gzencode($contenu);
    }

    try {
        $filesystem->write($fichier, $contenu);
        return true;
    } catch (\Throwable $th) {
        return false;
    }
}

/**
 * Supprimer un fichier de manière sympa (flock)
 *
 * @param string $fichier Chemin du fichier
 * @param bool   $lock    true pour utiliser un verrou
 *
 * @return bool
 *     - true si le fichier n'existe pas ou s'il a bien été supprimé
 *     - false si on n'arrive pas poser le verrou ou si la suppression échoue
 */
function supprimer_fichier($fichier, $lock = true): bool
{
    static $filesystem = null;

    if (is_null($filesystem)) {
        /** @var FilesystemInterface $filesystem */
        $filesystem = service('spip.filesystem');
    }

    try {
        $filesystem->remove($fichier);
        return true;
    } catch (\Throwable $th) {
        return false;
    }
}

/**
 * Supprimer brutalement un fichier ou un dossier, s'il existe
 *
 * @param string $f Chemin du fichier
 */
function spip_unlink($f)
{
    if (!is_dir($f)) {
        supprimer_fichier($f, false);
    } else {
        @unlink("$f/.ok");
        @rmdir($f);
    }
}

/**
 * Parcourt récursivement le repertoire `$dir`, et renvoie les
 * fichiers dont le chemin vérifie le pattern (preg) donné en argument.
 * 
 * @todo Refaire avec Finder
 *
 * En cas d'echec retourne un `array()` vide
 *
 * @example
 *     ```
 *     $x = preg_files('ecrire/data/', '[.]lock$');
 *     // $x array()
 *     ```
 *
 * @note
 *   Attention, afin de conserver la compatibilite avec les repertoires '.plat'
 *   si `$dir = 'rep/sous_rep_'` au lieu de `rep/sous_rep/` on scanne `rep/` et on
 *   applique un pattern `^rep/sous_rep_`
 *
 * @param string     $dir      Répertoire à parcourir
 * @param int|string $pattern  Expression régulière pour trouver des fichiers, tel que `[.]lock$`
 * @param int        $maxfiles Nombre de fichiers maximums retournés
 * @param array      $recurs   false pour ne pas descendre dans les sous répertoires
 *
 * @return array Chemins des fichiers trouvés.
 */
function preg_files($dir, $pattern = -1 /* AUTO */, $maxfiles = 10000, $recurs = [])
{
    $nbfiles = 0;
    if ($pattern == -1) {
        $pattern = '';
    }
    $fichiers = [];
    // revenir au repertoire racine si on a recu dossier/truc
    // pour regarder dossier/truc/ ne pas oublier le / final
    $dir = preg_replace(',/[^/]*$,', '', $dir);
    if ($dir == '') {
        $dir = '.';
    }

    if (@is_dir($dir) && is_readable($dir) && ($d = opendir($dir))) {
        while (($f = readdir($d)) !== false && ($nbfiles < $maxfiles)) {
            if (
                $f[0] != '.'
                && $f != 'remove.txt'
                && is_readable($f = "$dir/$f")
            ) {
                if (is_file($f)) {
                    if (!$pattern || preg_match(";$pattern;iS", $f)) {
                        $fichiers[] = $f;
                        $nbfiles++;
                    }
                } else {
                    if (is_dir($f) && is_array($recurs)) {
                        $rp = @realpath($f);
                        if (!is_string($rp) || !strlen($rp)) {
                            $rp = $f;
                        } # realpath n'est peut etre pas autorise
                        if (!isset($recurs[$rp])) {
                            $recurs[$rp] = true;
                            $beginning = $fichiers;
                            $end = preg_files(
                                "$f/",
                                $pattern,
                                $maxfiles - $nbfiles,
                                $recurs
                            );
                            $fichiers = array_merge((array)$beginning, (array)$end);
                            $nbfiles = count($fichiers);
                        }
                    }
                }
            }
        }
        closedir($d);
    }
    sort($fichiers);

    return $fichiers;
}

/**
 * Crée un sous répertoire
 *
 * Retourne `$base/{$subdir}/` si le sous-repertoire peut être crée
 *
 * @example
 *     ```
 *     sous_repertoire(_DIR_CACHE, 'demo');
 *     sous_repertoire(_DIR_CACHE . '/demo');
 *     ```
 *
 * @param string $base
 *     - Chemin du répertoire parent (avec $subdir)
 *     - sinon chemin du répertoire à créer
 * @param string $subdir
 *     - Nom du sous répertoire à créer,
 *     - non transmis, `$subdir` vaut alors ce qui suit le dernier `/` dans `$base`
 * @param bool $nobase
 *     true pour ne pas avoir le chemin du parent `$base/` dans le retour
 * @param bool $tantpis
 *     true pour ne pas raler en cas de non création du répertoire
 * @return string
 *     Chemin du répertoire créé.
 */
function sous_repertoire($base, $subdir = '', $nobase = false, $tantpis = false): string
{
    static $dirs = [];
    static $filesystem = null;

    if (is_null($filesystem)) {
        /** @var FilesystemInterface $filesystem */
        $filesystem = service('spip.filesystem');
    }

    $base = str_replace('//', '/', $base);

    # suppr le dernier caractere si c'est un /
    $base = rtrim($base, '/');

    if (!strlen($subdir)) {
        $n = strrpos($base, '/');
        if ($n === false) {
            return $nobase ? '' : ($base . '/');
        }
        $subdir = substr($base, $n + 1);
        $base = substr($base, 0, $n + 1);
    } else {
        $base .= '/';
        $subdir = str_replace('/', '', $subdir);
    }

    $baseaff = $nobase ? '' : $base;
    if (isset($dirs[$base . $subdir])) {
        return $baseaff . $dirs[$base . $subdir];
    }

    $path = $base . $subdir; # $path = 'IMG/distant/pdf' ou 'IMG/distant_pdf'

    if (file_exists("$path/.ok")) {
        return $baseaff . ($dirs[$base . $subdir] = "$subdir/");
    }

    $filesystem->mkdir($path, parameter('spip.chmod'));

    if (is_dir($path) && is_writable($path)) {
        @touch("$path/.ok");
        // spip_log("creation $base$subdir/");

        return $baseaff . ($dirs[$base . $subdir] = "$subdir/");
    }

    // en cas d'echec c'est peut etre tout simplement que le disque est plein :
    // l'inode du fichier dir_test existe, mais impossible d'y mettre du contenu
    spip_log("echec creation $base{$subdir}");
    if ($tantpis) {
        return '';
    }
    // FIXME: throw an Exception…
    // if (!_DIR_RESTREINT) {
    // 	$base = preg_replace(',^' . _DIR_RACINE . ',', '', $base);
    // }
    // $base .= $subdir;
    // raler_fichier($base . '/.ok');
}

/**
 * Suppression complete d'un repertoire.
 *
 * @param string $dir Chemin du repertoire
 *
 * @return bool Suppression reussie ou pas.
 */
function supprimer_repertoire(string $dir): bool
{
    /** @var FilesystemInterface $filesystem */
    $filesystem = service('spip.filesystem');
    try {
        $filesystem->remove($dir);
        return true;
    } catch (\Throwable $th) {
        return false;
    }
}

/**
 * Renvoie False si un fichier n'est pas plus vieux que $duree secondes.
 * 
 * sinon renvoie True et le date sauf si ca n'est pas souhaite
 */
function spip_touch(string $fichier, int $duree = 0, bool $touch = true): bool
{
    if ($duree) {
        clearstatcache();
        if (($f = @filemtime($fichier)) && $f >= time() - $duree) {
            return false;
        }
    }
    if ($touch !== false) {
        /** @var FilesystemInterface $filesystem */
        $filesystem = service('spip.filesystem');
        if (!$filesystem->touch($fichier)) {
            $filesystem->remove($fichier);
            $filesystem->touch($fichier);
        };
        @chmod($fichier, parameter('spip.chmod') & ~parameter('spip.umask'));
    }

    return true;
}
