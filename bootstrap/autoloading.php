<?php

use Spip\Runtime\Kernel;

/**
 * Cherche une fonction surchargeable et en retourne le nom exact,
 * après avoir chargé le fichier la contenant si nécessaire.
 *
 * Charge un fichier (suivant les chemins connus) et retourne si elle existe
 * le nom de la fonction homonyme `$dir_$nom`, ou suffixé `$dir_$nom_dist`
 *
 * Peut être appelé plusieurs fois, donc optimisé.
 *
 * @api
 * @uses include_spip() Pour charger le fichier
 * @example
 *     ```
 *     $envoyer_mail = charger_fonction('envoyer_mail', 'inc');
 *     $envoyer_mail($email, $sujet, $texte);
 *     ```
 *
 * @param string $nom
 *     Nom de la fonction (et du fichier)
 * @param string $dossier
 *     Nom du dossier conteneur
 * @param bool $continue
 *     true pour ne pas râler si la fonction n'est pas trouvée
 * @return string
 *     Nom de la fonction, ou false.
 */
function charger_fonction($nom, $dossier = 'exec', $continue = false)
{
    static $echecs = [];

    if (strlen($dossier) && !str_ends_with($dossier, '/')) {
        $dossier .= '/';
    }
    $f = str_replace('/', '_', $dossier) . $nom;

    if (function_exists($f)) {
        return $f;
    }
    if (function_exists($g = $f . '_dist')) {
        return $g;
    }

    if (isset($echecs[$f])) {
        return $echecs[$f];
    }
    // Sinon charger le fichier de declaration si plausible

    if (!preg_match(',^\w+$,', $f)) {
        if ($continue) {
            return false;
        } //appel interne, on passe
        // include_spip('inc/minipres');
        // echo minipres();
        // exit;
        throw new RuntimeException('erreur');
    }

    // passer en minuscules (cf les balises de formulaires)
    // et inclure le fichier
    if (
        !($inc = include_spip($dossier . ($d = strtolower($nom))))
        && strlen(dirname($dossier))
        && dirname($dossier) != '.'
    ) {
        include_spip(substr($dossier, 0, -1));
    }
    if (function_exists($f)) {
        return $f;
    }
    if (function_exists($g)) {
        return $g;
    }

    if ($continue) {
        return $echecs[$f] = false;
    }

    // Echec : message d'erreur
    spip_log("fonction $nom ($f ou $g) indisponible" .
        ($inc ? '' : " (fichier $d absent de $dossier)"));

    // include_spip('inc/minipres');
    // include_spip('inc/filtres_mini');
    // echo minipres(
    //     _T('forum_titre_erreur'),
    //     $inc ?
    //         _T('fonction_introuvable', ['fonction' => '<code>' . spip_htmlentities($f) . '</code>'])
    //         . '<br />'
    //         . _T('fonction_introuvable', ['fonction' => '<code>' . spip_htmlentities($g) . '</code>'])
    //         :
    //         _T('fichier_introuvable', ['fichier' => '<code>' . spip_htmlentities($d) . '</code>']),
    //     ['all_inline' => true,'status' => 404]
    // );
    // exit;
    throw new RuntimeException("fonction $nom ($f ou $g) indisponible" .
        ($inc ? '' : " (fichier $d absent de $dossier)"));
}

/**
 * Inclut un fichier PHP (en le cherchant dans les chemins)
 *
 * @api
 * @uses find_in_path()
 * @example
 *     ```
 *     include_spip('inc/texte');
 *     ```
 *
 * @param string $f
 *     Nom du fichier (sans l'extension)
 * @param bool $include
 *     - true pour inclure le fichier,
 *     - false ne fait que le chercher
 * @return string|bool
 *     - false : fichier introuvable
 *     - string : chemin du fichier trouvé
 */
function include_spip($f, $include = true)
{
    return find_in_path($f . '.php', '', $include);
}

/**
 * Requiert un fichier PHP (en le cherchant dans les chemins)
 *
 * @uses find_in_path()
 * @see  include_spip()
 * @example
 *     ```
 *     require_spip('inc/texte');
 *     ```
 *
 * @param string $f
 *     Nom du fichier (sans l'extension)
 * @return string|bool
 *     - false : fichier introuvable
 *     - string : chemin du fichier trouvé
 */
function require_spip($f)
{
    return find_in_path($f . '.php', '', 'required');
}

/**
 * Gestion des chemins (ou path) de recherche de fichiers par SPIP
 *
 * Empile de nouveaux chemins (à la suite de ceux déjà présents, mais avant
 * le répertoire `squelettes` ou les dossiers squelettes), si un répertoire
 * (ou liste de répertoires séparés par `:`) lui est passé en paramètre.
 *
 * Ainsi, si l'argument est de la forme `dir1:dir2:dir3`, ces 3 chemins sont placés
 * en tête du path, dans cet ordre (hormis `squelettes` & la globale
 * `$dossier_squelette` si définie qui resteront devant)
 *
 * Retourne dans tous les cas la liste des chemins.
 *
 * @note
 *     Cette fonction est appelée à plusieurs endroits et crée une liste
 *     de chemins finale à peu près de la sorte :
 *
 *     - dossiers squelettes (si globale précisée)
 *     - squelettes/
 *     - plugins (en fonction de leurs dépendances) : ceux qui dépendent
 *       d'un plugin sont devant eux (ils peuvent surcharger leurs fichiers)
 *     - racine du site
 *     - squelettes-dist/
 *     - prive/
 *     - ecrire/
 *
 * @param string|array $dir_path
 *     - Répertoire(s) à empiler au path
 *     - '' provoque un recalcul des chemins.
 * @return array
 *     Liste des chemins, par ordre de priorité.
 */
function _chemin($dir_path = null)
{
    static $kernel = null;
    static $path_base = null;
    static $path_full = null;

    if (is_null($kernel)) {
        /** @var Kernel $kernel */
        $kernel = service('spip.kernel');
    }
    $dirRacine = $kernel->getRootDir();
    $dossier_squelettes = $GLOBALS['dossier_squelettes'] ?? '';

    if ($path_base == null) {
        // Chemin standard depuis l'espace public
        $path = defined('_SPIP_PATH') ? constant('_SPIP_PATH') :
            $dirRacine . ':' .
            $dirRacine . '/squelettes-dist/:' .
            $dirRacine . '/prive/:' .
            $kernel->getRestrictedDir();
        // Ajouter squelettes/
        if (@is_dir($kernel->relative()->cwd . '/squelettes')) {
            $path = $kernel->relative()->cwd . '/squelettes/:' . $path;
        }
        foreach (explode(':', $path) as $dir) {
            if (strlen($dir) && !str_ends_with($dir, '/')) {
                $dir .= '/';
            }
            $path_base[] = $dir;
        }
        $path_full = $path_base;
        // Et le(s) dossier(s) des squelettes nommes
        if (strlen($dossier_squelettes)) {
            foreach (array_reverse(explode(':', $dossier_squelettes)) as $d) {
                array_unshift($path_full, ($d[0] == '/' ? '' : $dirRacine) . '/' . $d . '/');
            }
        }
        $GLOBALS['path_sig'] = md5(serialize($path_full));
    }
    if ($dir_path === null) {
        return $path_full;
    }

    if (is_array($dir_path) || strlen($dir_path)) {
        $tete = '';
        if (reset($path_base) == $dirRacine . 'squelettes/') {
            $tete = array_shift($path_base);
        }
        $dirs = (is_array($dir_path) ? $dir_path : explode(':', $dir_path));
        $dirs = array_reverse($dirs);
        foreach ($dirs as $dir_path) {
            if (!str_ends_with($dir_path, '/')) {
                $dir_path .= '/';
            }
            if (!in_array($dir_path, $path_base)) {
                array_unshift($path_base, $dir_path);
            }
        }
        if (strlen($tete)) {
            array_unshift($path_base, $tete);
        }
    }
    $path_full = $path_base;
    // Et le(s) dossier(s) des squelettes nommes
    if (strlen($dossier_squelettes)) {
        foreach (array_reverse(explode(':', $dossier_squelettes)) as $d) {
            array_unshift($path_full, ((isset($d[0]) && $d[0] == '/') ? '' : $dirRacine) . $d . '/');
        }
    }

    $GLOBALS['path_sig'] = md5(serialize($path_full));

    return $path_full;
}

/**
 * Retourne la liste des chemins connus de SPIP, dans l'ordre de priorité
 *
 * Recalcule la liste si le nom ou liste de dossier squelettes a changé.
 *
 * @uses _chemin()
 *
 * @return array Liste de chemins
 */
function creer_chemin()
{
    $path_a = _chemin();
    static $c = '';

    // on calcule le chemin si le dossier skel a change
    if ($c != ($GLOBALS['dossier_squelettes'] ?? '')) {
        // assurer le non plantage lors de la montee de version :
        $c = $GLOBALS['dossier_squelettes'] ?? '';
        $path_a = _chemin(''); // forcer un recalcul du chemin
    }

    return $path_a;
}

//
// chercher un fichier $file dans le SPIP_PATH
// si on donne un sous-repertoire en 2e arg optionnel, il FAUT le / final
// si 3e arg vrai, on inclut si ce n'est fait.
$GLOBALS['path_sig'] = '';
$GLOBALS['path_files'] = null;

/**
 * Recherche un fichier dans les chemins de SPIP (squelettes, plugins, core)
 *
 * Retournera le premier fichier trouvé (ayant la plus haute priorité donc),
 * suivant l'ordre des chemins connus de SPIP.
 *
 * @api
 * @see  charger_fonction()
 * @uses creer_chemin() Pour la liste des chemins.
 * @example
 *     ```
 *     $f = find_in_path('css/perso.css');
 *     $f = find_in_path('perso.css', 'css');
 *     ```
 *
 * @param string $file
 *     Fichier recherché
 * @param string $dirname
 *     Répertoire éventuel de recherche (est aussi extrait automatiquement de $file)
 * @param bool|string $include
 *     - false : ne fait rien de plus
 *     - true : inclut le fichier (include_once)
 *     - 'require' : idem, mais tue le script avec une erreur si le fichier n'est pas trouvé.
 * @return string|bool
 *     - string : chemin du fichier trouvé
 *     - false : fichier introuvable
 */
function find_in_path($file, $dirname = '', $include = false)
{
    static $kernel = null;
	static $dirs = [];
	static $inc = []; # cf https://git.spip.net/spip/spip/commit/42e4e028e38c839121efaee84308d08aee307eec
	static $c = '';

    if (is_null($kernel)) {
        /** @var Kernel $kernel */
        $kernel = service('spip.kernel');
    }

	if (!$file && !strlen($file)) {
        // var_dump('file is  emtpy.');
		return false;
    }

	// on calcule le chemin si le dossier skel a change
	if ($c != ($GLOBALS['dossier_squelettes'] ?? '')) {
		// assurer le non plantage lors de la montee de version :
		$c = $GLOBALS['dossier_squelettes'];
		creer_chemin(); // forcer un recalcul du chemin et la mise a jour de path_sig
	}

	if (isset($GLOBALS['path_files'][$GLOBALS['path_sig']][$dirname][$file])) {
		if (!$GLOBALS['path_files'][$GLOBALS['path_sig']][$dirname][$file]) {
			return false;
		}
		if ($include && !isset($inc[$dirname][$file])) {
			include_once /**$kernel->cwd . */$GLOBALS['path_files'][$GLOBALS['path_sig']][$dirname][$file];
			$inc[$dirname][$file] = $inc[''][$dirname . $file] = true;
		}

		return $GLOBALS['path_files'][$GLOBALS['path_sig']][$dirname][$file];
	}

	$a = strrpos($file, '/');
	if ($a !== false) {
		$dirname .= substr($file, 0, ++$a);
		$file = substr($file, $a);
	}

	foreach (creer_chemin() as $dir) {
		if (!isset($dirs[$a = $dir . $dirname])) {
			$dirs[$a] = (is_dir(/**$kernel->cwd . */$a) || !$a);
		}
		if ($dirs[$a]) {
			if (file_exists(/**$kernel->cwd . */($a .= $file))) {
				if ($include && !isset($inc[$dirname][$file])) {
					include_once /**$kernel->cwd . */$a;
					$inc[$dirname][$file] = $inc[''][$dirname . $file] = true;
				}
				if (!defined('_SAUVER_CHEMIN')) {
					// si le chemin n'a pas encore ete charge, ne pas lever le flag, ne pas cacher
					if (is_null($GLOBALS['path_files'])) {
						return $a;
					}
					define('_SAUVER_CHEMIN', true);
				}

				return $GLOBALS['path_files'][$GLOBALS['path_sig']][$dirname][$file] = $GLOBALS['path_files'][$GLOBALS['path_sig']][''][$dirname . $file] = $a;
			}
		}
	}

	if ($include) {
		spip_log("include_spip $dirname$file non trouve");
		if ($include === 'required') {
			echo '<pre>',
			'<strong>Erreur Fatale</strong><br />';
			if (function_exists('debug_print_backtrace')) {
				debug_print_backtrace();
			}
			echo '</pre>';
			die("Erreur interne: ne peut inclure $dirname$file");
		}
	}

	if (!defined('_SAUVER_CHEMIN')) {
		// si le chemin n'a pas encore ete charge, ne pas lever le flag, ne pas cacher
		if (is_null($GLOBALS['path_files'])) {
			return false;
		}
		define('_SAUVER_CHEMIN', true);
	}

	return $GLOBALS['path_files'][$GLOBALS['path_sig']][$dirname][$file] = $GLOBALS['path_files'][$GLOBALS['path_sig']][''][$dirname . $file] = false;
}

/**
 * Trouve tous les fichiers du path correspondants à un pattern
 *
 * Pour un nom de fichier donné, ne retourne que le premier qui sera trouvé
 * par un `find_in_path()`
 *
 * @api
 * @uses creer_chemin()
 * @uses preg_files()
 *
 * @param string $dir
 * @param string $pattern
 * @param bool $recurs
 * @return array
 */
function find_all_in_path($dir, $pattern, $recurs = false)
{
	$liste_fichiers = [];
	$maxfiles = 10000;

	// Parcourir le chemin
	foreach (creer_chemin() as $d) {
		$f = $d . $dir;
		if (@is_dir($f)) {
			$liste = preg_files($f, $pattern, $maxfiles - count($liste_fichiers), $recurs === true ? [] : $recurs);
			foreach ($liste as $chemin) {
				$nom = basename($chemin);
				// ne prendre que les fichiers pas deja trouves
				// car find_in_path prend le premier qu'il trouve,
				// les autres sont donc masques
				if (!isset($liste_fichiers[$nom])) {
					$liste_fichiers[$nom] = $chemin;
				}
			}
		}
	}

	return $liste_fichiers;
}
