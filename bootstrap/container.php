<?php

use Monolog\Level;
use Spip\Component\Filesystem\Filesystem;
use Spip\Component\Logger\Config;
use Spip\Component\Logger\Factory;
use Spip\Component\Logger\LineFormatter;
use Spip\Runtime\Installation;
use Spip\Runtime\Kernel;
use Spip\Runtime\Old;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;
use Symfony\Component\DependencyInjection\Reference;

function container(): ContainerInterface
{
    static $container = null;

    if (is_null($container)) {
        $env = getenv('APP_ENV') ?? 'prod';
        $installation = Installation::fromComposer();
        $loggerConfig = [
            'siteDir' => $installation->getSiteDir(),
            'filesystem' => new Reference('spip.filesystem'),
        ];
        foreach([
            'max_log' => '_MAX_LOG',
            'fileline' => '_LOG_FILELINE',
            'brut' => '_LOG_BRUT',
            'max_level' => '_LOG_FILTRE_GRAVITE',
        ] as $name => $constantName) {
            $loggerConfig[$name] = Old::constant($constantName);
        }
        $loggerConfig = array_merge($loggerConfig, $env == 'dev' ? [
            'max_level' => Level::Debug,
            'fileline' => true,
        ] : []);
        foreach([
            'max_files' => ['nombre_de_logs', 4],
            'max_size' => ['taille_des_logs',102_400],
        ] as $name => $globalName) {
            $loggerConfig[$name] = Old::global(...$globalName);
        }
        $loggerConfig['log_path'] = str_replace('spip', '%s', convertOldConstantsToLogPath());
        $loggerConfig = array_filter($loggerConfig);

        $kernelConfig = [
            'installation' => $installation,
        ];
        foreach([
            'restricted' => '_DIR_RESTREINT_ABS',
            'etc' => '_DIR_ETC',
            'tmp' => '_DIR_TMP',
            'img' => '_DIR_IMG',
            'var' => '_DIR_VAR',
        ] as $name => $constantName) {
            $kernelConfig[$name] = Old::constant($constantName);
        }
        $kernelConfig = array_filter($kernelConfig);

        $parameters = new FrozenParameterBag([
            // 'spip.env' => '%env(APP_ENV)%',
            // Calculé à l'install puis immutable
            'spip.chmod' => 0777, // @todo traiter la constante _SPIP_CHMOD
            'spip.umask' => 0111, // sprintf('%04o', $chmod & ~$umask) => 0666
            'spip.project_dir' => $installation->getProjectDir(),
            'spip.site_dir' => $installation->getSiteDir(),
            'spip.web_dir' => $installation->getWebDir(),

            'spip.logger.default_level' => Level::Info,
        ]);
        $container = new ContainerBuilder($parameters);
        $container->register('spip.kernel', Kernel::class)
            ->setArguments($kernelConfig);
        $container->register('spip.filesystem', Filesystem::class);
        $container->register('spip.logger.formatter', LineFormatter::class);
        $container->register('spip.logger.config', Config::class)
            ->setArguments($loggerConfig);
        $container->register('spip.logger.factory', Factory::class)
            ->addArgument(new Reference('spip.logger.config'))
            ->addArgument(new Reference('spip.logger.formatter'));
    }

    return $container;
}

/**
 * Raccourci temporaire pour accéder à un paramètre du container symfony.
 */
function parameter(string $name): mixed
{
    return container()->getParameter($name);
}

/**
 * Raccourci temporaire pour accéder à un service du container symfony.
 */
function service($id): ?object
{
    return container()->get($id);
}
